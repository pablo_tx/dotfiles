# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

source /usr/share/zsh/share/antigen.zsh

# Languages
LANG="es_ES.UTF-8"
LC_COLLATE="es_ES.UTF-8"
LC_CTYPE="es_ES.UTF-8"
LC_MESSAGES="es_ES.UTF-8"
LC_MONETARY="es_ES.UTF-8"
LC_NUMERIC="es_ES.UTF-8"
LC_TIME="es_ES.UTF-8"
LC_ALL=

antigen use oh-my-zsh

# Bundles
antigen bundles <<EOBUNDLES
  rupa/z@master
  reem/watch@master
  zsh-users/zsh-syntax-highlighting
  zsh-users/zsh-autosuggestions@master # do not use develop
  zsh-users/zsh-completions
  psprint/zsh-navigation-tools
  psprint/zsh-select                   # required by zsh-ctrlp
  garybernhardt/selecta@v0.0.*
  desyncr/zsh-icdiff
  hlissner/zsh-autopair
  supercrabtree/k@0.*
  LuRsT/hr
  ddollar/git-utils                    # various git utilities
  docker
  arzzen/calc.plugin.zsh
EOBUNDLES

antigen theme romkatv/powerlevel10k

# Tell antigen that you're done.
antigen apply

ZSH_AUTOSUGGEST_CLEAR_WIDGETS+=(
    "expand-or-complete"
    "pcomplete"
    "copy-earlier-word"
)

# History settings
export    SAVEHIST=30000
export    HISTDUP=erase               #Erase duplicates in the history file
setopt    append_history	#Append history to the history file (no overwriting)
setopt    share_history		#Share history across terminals
setopt    inc_append_history	#Immediately append to the history file, not just when a term is killed
setopt    hist_ignore_all_dups	# Delete old recorded entry if new entry is a duplicate.

#
# Arch exports for android tools
#
#export ANDROID_HOME=/opt/android-sdk
#export PATH=$PATH:$ANDROID_HOME/emulator
#export PATH=$PATH:$ANDROID_HOME/tools
#export PATH=$PATH:$ANDROID_HOME/tools/bin
#export PATH=$PATH:$ANDROID_HOME/platform-tools

# Wine prefix
export WINEPREFIX=~/.wine

# Docker
export DOCKER_BUILDKIT=1
export COMPOSE_DOCKER_CLI_BUILD=1

# LSD alias
alias ls='lsd'

# Additional bin locations
export PATH=$PATH:~/go/bin

# Kubeconfig
#export KUBECONFIG=

# P10K theme
# To customize prompt, run p10k configure or edit ~/.p10k.zsh.
[[ -f ~/.p10k.zsh ]] && source ~/.p10k.zsh
